// Initialize leaflet.js
var L = require('leaflet');
var lT = require('./layerTree');
var navBar = require('leaflet-navbar');
var visualclick = require('leaflet.visualclick');
var zoomBox = require('leaflet-zoombox');
var leyenda = require('leaflet-html-legend');



var layerControlOptions = {
	container_width 	: "300px",
	group_maxHeight     : "80px",
	container_maxHeight : "350px", 
	exclusive       	: false,
	collapsed : true, 
	position: 'topleft',
	
};
// Initialize the map
var map = L.map('map', {
  scrollWheelZoom: false,
  attributionControl: false,
	visualClickEvents: 'click contextmenu',
	zoomControl: false 
  
});

// Set the position and zoom level of the map
map.setView([-25.3155,-57.4475], 10);
// Plugins
L.control.zoom().addTo(map);
L.control.navbar({position: 'topleft'}).addTo(map);
L.control.scale().addTo(map);
//L.control.pan().addTo(map);



var options = {
    modal: true,
    title: "Box area zoom"
};
var control = L.control.zoomBox(options);
map.addControl(control);





/* Base Layers */
var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	//attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
}).addTo(map);

var esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	//attribution: 'Tiles &copy; Esri &mdash; Source: USGS, Esri, TANA, DeLorme, and NPS',
	//maxZoom: 13
});

var esri_NatGeoWorldMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}', {
	//attribution: 'Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC',
	//maxZoom: 16
});
/* Overlays Layers */
var conductividad = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:conductividad_hidra", format:'image/png',transparent:true});
	
var topografia = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:tipo_suelo", format:'image/png',transparent:true});

var recarga = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:recarga1", format:'image/png',transparent:true});

var mapa_riesgo = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:riesgo_nt", format:'image/png',transparent:true});

var profundidad_napa = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:profundidad_napa_2013", format:'image/png',transparent:true});

var nitrogeno2010 = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:nitrogeno_2010", format:'image/png',transparent:true});

var uso_suelo = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:uso_suelo_ok", format:'image/png',transparent:true});

var coliformes2010 = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:concentracion_coliformes_2010", format:'image/png',transparent:true});

var coliformes2006 = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:concetracion_coliformes_2006", format:'image/png',transparent:true});
	
var nitrogeno2006 = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:concentracion_nitrogeno_2006", format:'image/png',transparent:true});

var area_total = L.tileLayer.wms("http://localhost:8080/geoserver/patinho/wms",
	{layers:"patinho:area_total2", format:'image/png',transparent:true});//.addTo(map);
	


var baseMaps = [
	
		
                { 
					groupName : "Base Layers",
					expanded : true,
					layers    : {
						"OpenStreetMaps" : osm ,
						"ESRI World Imagery": esri_WorldImagery,
						"ESRI National Geographic": esri_NatGeoWorldMap
					}
		        }							
];

var overlays = [
				 {
					groupName : "Layers",
					expanded : true,
					layers    : { 
						"Mapa de riesgo general"							: area_total,
						"Profundidad de la napa freática. Año 2013 "		: profundidad_napa,
						"(C) Conductividad Hidráulica"						: conductividad,
						"(R) Recarga"			   							: recarga,
						"(T) Topografía del terreno" 						: topografia,
						"Mapa de riesgo de contaminación por Nitrógeno total" :mapa_riesgo,
						"Concentración de Nitrógenos totales del año 2006 (mg/L)" :nitrogeno2006,
						"Concentración de Nitrógenos totales del año 2010 (mg/L)" :nitrogeno2010,
						"Parámetro antropogénico: (L) Uso de la tierra" 	:uso_suelo,
						"Concentración de Coliformes totales del año 2006 (UFC/100ML)" 	:coliformes2006,
						"Concentración de Coliformes totales del año 2010 (UFC/100ML)" 	:coliformes2010,

					},
						
                 }
                

];
//para que la capa sea removible y visible
/*area_total.StyledLayerControl = {
	removable : false,
	visible : true
};*/

var control = L.Control.styledLayerControl(baseMaps, overlays, layerControlOptions);

map.addControl(control);
control.selectLayer(area_total);  // seleccion por defecto de capa


/* legend */
var Legend = L.control.htmllegend({
	position: 'bottomright',
	legends: [{
		name: 'Mapa de riesgo general (Nt+Ct)',
		layer: area_total,
		elements: [{
			label: 'Valores de capa',
			html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=area_total2" width="45" height="100" />',
	 
			style: ''

		}]
	}],
	collapseSimple: true,
	detectStretched: true,
	//collapsedOnInit: true, mantener cerrada la leyenda de los raster
	defaultOpacity: 0.7,
	visibleIcon: 'icon icon-eye',
	hiddenIcon: 'icon icon-eye-slash',
	
	
});
map.addControl(Legend);
	
Legend.addLegend({
	name: 'Profundidad de la napa freática. Año 2013 ',
	layer: profundidad_napa,
	opacity: 0.5,
	elements: [{
	 html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=profundidad_napa_2013" width="20" height="20" />',
	 
		
		style: ''
	}]
});

Legend.addLegend({
	name: '(C)Conductividad Hidráulica',
	layer: conductividad,
	opacity: 0.5,
	elements: [{
		label: 'Valores de capa',
			html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=conductividad_hidra" width="45" height="100" />',
	 
			style: ''
	}]
});


Legend.addLegend({
	name: '(R) Recarga',
	layer: recarga,
	opacity: 0.5,
	elements: [{
		label: 'Valores de capa',
			html:  '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=recarga1" width="45" height="100" />',
	 
			style: ''
	}]

});

Legend.addLegend({
	name: '(T) Topografía del terreno',
	layer: topografia,
	opacity: 0.5,
	elements: [{
		label: 'Valores de capa',
		html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=tipo_suelo" width="45" height="100" />',
	 
		style: '',
}]
});


Legend.addLegend({
	name: 'Parámetro antropogénico: (L) Uso de la tierra',
	layer: uso_suelo,
	opacity: 0.5,
	elements: [{
		label: 'Valores de capa',
			html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=uso_suelo_ok" width="45" height="100" />',
	 
			style: ''
	}]

});

Legend.addLegend({
	name: 'Mapa de riesgo de contaminación por Nitrógeno total',
	layer: mapa_riesgo,
	opacity: 0.5,
	elements: [{
		label: 'Valores de capa',
			html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=riesgo_nt" width="45" height="100" />',
	 
			style: '',
	}]
});



Legend.addLegend({
	name: 'Concentración de Nitrógenos totales del año 2006 (mg/L)',
	layer: nitrogeno2006,
	opacity: 0.5,
	elements: [{
		html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=concentracion_nitrogeno_2006" width="20" height="20" />',
	 
		style: ''
	}]
});

Legend.addLegend({
	name: 'Concentración de Nitrógenos totales del año 2010 (mg/L)',
	layer: nitrogeno2010,
	opacity: 0.5,
	elements: [{
		html: '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=nitrogeno_2010" width="20" height="20" />',
		
		style: ''
	}]
});


Legend.addLegend({
	name: 'Concentración de Coliformes totales del año 2006 (UFC/100ML)',
	layer: coliformes2006,
	opacity: 0.5,
	elements: [{
		html:  '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=concetracion_coliformes_2006" width="20" height="20" />',
	 
		style: ''
	}]
})


Legend.addLegend({
	name: 'Concentración de Coliformes totales del año 2010 (UFC/100ML)',
	layer: coliformes2010,
	opacity: 0.5,
	elements: [{
		html:  '<img alt="legend" src=" http://localhost:8080/geoserver/patinho/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=concentracion_coliformes_2010" width="20" height="20" />',
	 
		style: ''
	}]
});